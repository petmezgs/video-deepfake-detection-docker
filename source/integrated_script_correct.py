import argparse
import subprocess
import os
import stat
import cv2
import shutil
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras import backend as K

'''
# Function to print the active conda environment and pip list
def print_conda_env_and_pip_list():
    # Print the active conda environment
    conda_env = subprocess.check_output(["conda", "env", "list"]).decode("utf-8")
    print(f"Active Conda Environment:\n{conda_env}")

    # Run pip list
    try:
        result = subprocess.run(['pip', 'list'], capture_output=True, text=True, check=True)
        print(result.stdout)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e.stderr}")
'''

# Function to compute Euclidean distance
def euclidean_distance(inputs):
    assert len(inputs) == 2, f'Expected 2 inputs, got {len(inputs)}'
    x, y = inputs
    return np.linalg.norm(x - y)

def truncate_number(n, decimals=3):
    factor = 10.0 ** decimals
    return np.trunc(n * factor) / factor

# Function to process sequences to the desired shape
def process_sequences(folder_path, input_shape):
    sequences = []
    npy_files = sorted([f for f in os.listdir(folder_path) if f.startswith("frame") and f.endswith(".npy")],
                       key=lambda x: int(x[5:-4]))

    sequence_length = 100  # Length of each sequence
    overlap = sequence_length // 4  # Overlap size - 75%

    for i in range(0, len(npy_files) - sequence_length + 1, overlap):
        #sequence = [np.load(os.path.join(folder_path, npy_files[i + j]))[3:] for j in range(sequence_length)]
        sequence = [truncate_number(np.load(os.path.join(folder_path, npy_files[i + j]))[3:], 3) for j in range(sequence_length)]
        sequences.append(sequence)

    # Convert list of sequences to numpy array and reshape to desired shape
    processed_sequences = np.array(sequences)
    return processed_sequences.reshape(-1, *input_shape)

# Function to extract frames from videos
def extract_frames(video_path, output_directory):
    cap = cv2.VideoCapture(video_path)
    frame_count = 0

    os.makedirs(output_directory, exist_ok=True)

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        frame_filename = os.path.join(output_directory, f"frame{frame_count}.jpg")
        cv2.imwrite(frame_filename, frame)

        frame_count += 1

    cap.release()

# Function to process video frames and execute DECA-related subprocess
def process_video(input_path, output_path):
    print("DECA Script started")

    # Create output directories
    os.makedirs(os.path.join(output_path, 'frames'), exist_ok=True)
    os.makedirs(os.path.join(output_path, 'DECA'), exist_ok=True)

    # Execute frame extraction for all videos in the input directory
    for video_file in os.listdir(input_path):
        if video_file.endswith(".mp4"):
            video_path = os.path.join(input_path, video_file)

            #print('video_path is: ', video_path)
            # Output directories for frames and DECA parameters
            output_directory_frames = os.path.join(output_path, 'frames', video_file.replace(".mp4", ""))
            output_directory_params = os.path.join(output_path, 'DECA', video_file.replace(".mp4", ""))

            # Create the directories if they don't exist
            os.makedirs(output_directory_frames, exist_ok=True)
            os.makedirs(output_directory_params, exist_ok=True)

            # Change permissions of the output_directory_frames
            subprocess.run(['chmod', '+x', '-R', output_directory_frames])
            # Change permissions of the output_directory_params
            subprocess.run(['chmod', '+x', '-R', output_directory_params])

            # Extract frames from the video
            extract_frames(video_path, output_directory_frames)


            '''
            # Print the contents and permissions of the frames folder and its subfolders
            print(f"Contents and Permissions of {output_directory_frames}:")
            for root, dirs, files in os.walk(output_directory_frames):
                print(f"  {root}")
                for file_name in files:
                    file_path = os.path.join(root, file_name)
                    permissions = stat.filemode(os.stat(file_path).st_mode)
                    print(f"    {file_name} - Permissions: {permissions}")
            '''

            # Execute DECA-related script using subprocess
            subprocess.run([
                "python", "source/demo_reconstruct_save_parameters.py",
                "-i", output_directory_frames, "-s", output_directory_params
            ])

            '''
            # Print the contents and permissions of the DECA output folder and its subfolders
            print(f"Contents and Permissions of {output_directory_params}:")
            for root, dirs, files in os.walk(output_directory_params):
                print(f"  {root}")
                for file_name in files:
                    file_path = os.path.join(root, file_name)
                    permissions = stat.filemode(os.stat(file_path).st_mode)
                    print(f"    {file_name} - Permissions: {permissions}")
            '''
           

    output_directory_deca = os.path.join(output_path, 'DECA')
    print("DECA Script completed")
    return output_directory_deca


# Function to process video frames and execute DL model
def process_video_with_dl_model(data_directory, input_shape, threshold_distance):
    print("DL Model Script started")

    # Load the trained Siamese network and the shared network
    trained_model = load_model('model/siamese_net_triplet_2024.h5', compile=False)
    branch_model = trained_model.get_layer('shared_network')
    input = Input(shape=input_shape)
    x = branch_model(input)
    base_network = Model(input, x)
    base_network.summary()

    reference_folder = [folder for folder in os.listdir(data_directory) if folder.startswith("Reference")]
    reference_sequences = {}
    for folder in reference_folder:
        folder_path = os.path.join(data_directory, folder)
        #print(f"Processing Reference Folder: {folder_path}")

        # Check if folder contains .npy files
        #npy_files = [f for f in os.listdir(folder_path) if f.endswith(".npy")]
        #print(f"Found .npy files in Reference Folder: {npy_files}")

        reference_sequences[folder] = process_sequences(folder_path, input_shape)
        #print(f"Reference Sequences for {folder}: {reference_sequences[folder].shape}")

    # Load and process test sequences
    test_folder = [folder for folder in os.listdir(data_directory) if folder.startswith("Test")]
    test_sequences = {}
    for folder in test_folder:
        folder_path = os.path.join(data_directory, folder)
        #print(f"Processing Test Folder: {folder_path}")

        # Check if folder contains .npy files
        npy_files = [f for f in os.listdir(folder_path) if f.endswith(".npy")]
        #print(f"Found .npy files in Test Folder: {npy_files}")

        test_sequences[folder] = process_sequences(folder_path, input_shape)
        #print(f"Test Sequences for {folder}: {test_sequences[folder].shape}")

    '''
    # Print reference_sequences and test_sequences
    print("\nReference Sequences:")
    for folder_name, sequences in reference_sequences.items():
        print(f"{folder_name}: {sequences.shape}")

    print("\nTest Sequences:")
    for folder_name, sequences in test_sequences.items():
        print(f"{folder_name}: {sequences.shape}")
    '''

    # Calculate distances between each test sequence and all reference sequences for each test video
    num_distances_below_threshold = {}

    for test_folder_name, test_seq in sorted(test_sequences.items(), key=lambda x: int(x[0].split('Test')[1])):
        count_distances_below_threshold = 0
        total_test_seq = len(test_seq)
        total_ref_seq = 0

        for ref_folder_name, ref_seq in sorted(reference_sequences.items(), key=lambda x: int(x[0].split('Reference')[1])):
            individual_ref_seq = len(ref_seq)

            for i, test_sequence in enumerate(test_seq):
                for j, ref_sequence in enumerate(ref_seq):
                    distance = euclidean_distance([
                        branch_model.predict(np.expand_dims(test_sequence, axis=0))[0],
                        branch_model.predict(np.expand_dims(ref_sequence, axis=0))[0]
                    ])

                    if distance < threshold_distance:  # Threshold for distance
                        count_distances_below_threshold += 1
            total_ref_seq += individual_ref_seq

        # Calculate percentage of distances below the threshold for each test video
        percentage_below_threshold = (count_distances_below_threshold / (total_test_seq * total_ref_seq)) * 100
        num_distances_below_threshold[test_folder_name] = percentage_below_threshold

    '''
    for test_folder_name, percentage in num_distances_below_threshold.items():
        print(f"Percentage of distances below {threshold_distance} for Test '{test_folder_name}': {percentage:.2f}%")
    '''
    with open('/app/data/output/results.txt', 'w') as file:
        pass  # This will clear the file
    with open('/app/data/output/results.txt', 'a') as file:
        for test_folder_name, percentage in num_distances_below_threshold.items():
            file.write(f"Percentage of distances below {threshold_distance} for Test '{test_folder_name}': {percentage:.2f}%\n")

    print("DL Model Script completed")

if __name__ == "__main__":
    # Print the active conda environment and pip list
    #print_conda_env_and_pip_list()

    # Uninstall opencv-python
    subprocess.run(['pip', 'uninstall', 'opencv-python', '-y'], check=True)

    # Uninstall opencv-python-headless
    subprocess.run(['pip', 'uninstall', 'opencv-python-headless', '-y'], check=True)

    # Install opencv-python-headless
    subprocess.run(['pip', 'install', 'opencv-python-headless'], check=True)

    # Print the active conda environment and pip list
    #print_conda_env_and_pip_list()

    parser = argparse.ArgumentParser(description='Combined Video Processing with DECA and DL Model Prediction')
    parser.add_argument('--input-dir', type=str, help='Path to the directory containing input videos')
    parser.add_argument('--output-dir', type=str, help='Path to the directory for output frames and processed data')
    args = parser.parse_args()

    if not args.input_dir or not args.output_dir:
        parser.error('Please provide input and output directories')

    input_dir = args.input_dir
    output_dir = args.output_dir

    # Set your desired threshold here
    threshold_distance = 0.055

    # Run DECA script to extract frames and save parameters
    deca_output_dir = process_video(input_dir, output_dir)

    # Run DL model script to process sequences and calculate distances
    process_video_with_dl_model(deca_output_dir, (100, 156), threshold_distance)

    print("Combined Script completed")