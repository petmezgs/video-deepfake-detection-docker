from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import chumpy as ch
from smpl_webuser.serialization import load_model
from smpl_webuser.verts import verts_decorated
from psbody.mesh import Mesh
import sys
import argparse

import sys
import os
from absl import flags
import numpy as np
import skimage.io as io
import cv2
import matplotlib.pyplot as plt
import tensorflow as tf
from psbody.mesh import Mesh
from smpl_webuser.serialization import load_model

from util import renderer as vis_util
from util import image as img_util
from util.project_on_mesh import compute_texture_map
from config_test import get_config
from run_RingNet import RingNet_inference

import cv2
print(cv2.__version__)

def make_prdicted_mesh_neutral_deca(predicted_params_path, config):
	params = np.load(predicted_params_path, allow_pickle=True)

	for row in range(0,np.shape(params)[0]):

		print(row)

		pose,betas = np.zeros(15),np.zeros(400)
		pose[:3],pose[6:9]= params[row][3:6],params[row][6:9]
		betas[:100] = params[row][9:109]
		betas[300:350] = params[row][109:159]	


		flame_genral_model = load_model(config.flame_model_path)
		generated_neutral_mesh = verts_decorated(ch.array([0,0,0]),
			            ch.array(pose),
			            ch.array(flame_genral_model.r),
			            flame_genral_model.J_regressor,
			            ch.array(flame_genral_model.weights),
			            flame_genral_model.kintree_table,
			            flame_genral_model.bs_style,
			            flame_genral_model.f,
			            bs_type=flame_genral_model.bs_type,
			            posedirs=ch.array(flame_genral_model.posedirs),
			            betas=ch.array(betas),#betas=ch.array(np.concatenate((theta[0,75:85], np.zeros(390)))), #
			            shapedirs=ch.array(flame_genral_model.shapedirs),
			            want_Jtr=True)
		neutral_mesh = Mesh(v=generated_neutral_mesh.r, f=generated_neutral_mesh.f)
		out_mesh_fname = '/home/petmezgs/Documents/EITHOS/DECA/Results/3DMMs/testingDECA/reconstructed_3dmms/frame_{}.obj'.format(row)
		neutral_mesh.write_obj(out_mesh_fname)



if __name__=="__main__":
    
    print("aba")
    a = argparse.ArgumentParser()
    a.add_argument("--predicted_params_path", help="path to predicted FLAME parameters")
    #predicted_params_path = '/home/petmezgs/Documents/GitHub/RingNet/RingNet_output/params/fused_parameters.npy'
    #flame_model_path = '/home/petme/Documents/Github/RingNet-master/flame_model/generic_model.pkl'
    #print(flame_model_path)
    #a.add_argument("--flame_model_path", help="path to FLAME model")
    args = a.parse_args()
    print(args)
    print(args.predicted_params_path)
    config = get_config()
    make_prdicted_mesh_neutral_deca(args.predicted_params_path, config) #args.flame_model_path