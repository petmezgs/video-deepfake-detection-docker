import argparse
import subprocess
import os

'''
# Print the active conda environment
conda_env = subprocess.check_output(["conda", "env", "list"]).decode("utf-8")
#print(f"Active Conda Environment:\n{conda_env}")
'''
'''
# Run pip list
try:
    result = subprocess.run(['pip', 'list'], capture_output=True, text=True, check=True)
    print(result.stdout)
except subprocess.CalledProcessError as e:
    print(f"Error: {e.stderr}")
'''

# Uninstall opencv-python
subprocess.run(['pip', 'uninstall', 'opencv-python', '-y'], check=True)

# Uninstall opencv-python-headless
subprocess.run(['pip', 'uninstall', 'opencv-python-headless', '-y'], check=True)

# Install opencv-python-headless
subprocess.run(['pip', 'install', 'opencv-python-headless'], check=True)

'''
# Run pip list
try:
    result = subprocess.run(['pip', 'list'], capture_output=True, text=True, check=True)
    print(result.stdout)
except subprocess.CalledProcessError as e:
    print(f"Error: {e.stderr}")
'''

def parse_arguments():
    parser = argparse.ArgumentParser(description="ID Network Inference Interactive Interface")
    parser.add_argument('--input-dir', type=str, help='Path to the directory containing input videos')
    parser.add_argument('--output-dir', type=str, help='Path to the directory for output frames and processed data')
    return parser.parse_args()
      
def main(input_dir, output_dir):
    print("Script started")

    # Check if the provided input directory exists
    if not os.path.exists(input_dir):
        print(f"Error: Input directory '{input_dir}' does not exist.")
        return

    # Execute inference_arg.py with the input and output directory arguments
    subprocess.run(["python3", "source/integrated_script_correct.py", "--input-dir", input_dir, "--output-dir", output_dir])

    print("Script executed successfully.")

if __name__ == "__main__":
    args = parse_arguments()
    main(args.input_dir, args.output_dir)
