#!/bin/bash
# Start Xvfb in the background
Xvfb :99 -screen 0 1024x768x16 &
# Set the DISPLAY variable
export DISPLAY=:99
# Activate the virtual environment
source /app/virtualenv/bin/activate
# Run your Python script
python interactive_interface.py

