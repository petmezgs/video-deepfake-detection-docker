# Step 1: Build the image
FROM continuumio/miniconda3:latest

# Copy all files from the build context to /app/ in the Docker image
COPY . /app/

# Set working directory to /app/
WORKDIR /app/

# Recreate conda environment
RUN conda env create -f /app/env/environment.yml && conda clean -afy

# Set the conda environment to activate by default
SHELL ["/bin/bash", "-c", "source activate integ_env && conda run -n integ_env /bin/bash"]

# Download the model checkpoint and place it in the desired directory

#RUN wget -O 2DFAN4-cd938726ad.zip https://www.adrianbulat.com/downloads/python-fan/2DFAN4-cd938726ad.zip
# Copy the model checkpoint into the image
WORKDIR /root/.cache/torch/hub/checkpoints
COPY zip/2DFAN4-cd938726ad.zip /root/.cache/torch/hub/checkpoints/


WORKDIR /app

# Copy the s3fd-619a316812.pth file used for FAN
COPY source/s3fd-619a316812.pth /root/.cache/torch/hub/checkpoints/s3fd-619a316812.pth

# Install additional dependencies for rendering with OpenCV
RUN apt-get update && apt-get install -y --no-install-recommends libgl1-mesa-glx
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 -y && apt-get install libgl1
RUN apt-get update && apt-get install -y libgl1-mesa-dev

# Set execute permission for the Python script
RUN chmod +x /app/source/interactive_interface.py

# Specify the command and its arguments, activate the environment, and run the script
ENTRYPOINT ["/bin/bash", "-c", "source activate integ_env && pip uninstall opencv-python -y && python3 source/interactive_interface.py --input-dir /app/data/input --output-dir /app/data/output"]
